FROM openjdk:8-jdk-alpine
VOLUME /tmp
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
CMD /bin/sh -c "java -XX:MinHeapFreeRatio=${JAVA_MIN_HEAP_FREE_RATIO:=10} -XX:MaxHeapFreeRatio=${JAVA_MAX_HEAP_FREE_RATIO:=70} -XX:CompressedClassSpaceSize=${JAVA_COMPRESSED_CLASS_SPACE_SIZE:=64m} -XX:ReservedCodeCacheSize=${JAVA_RESERVED_CODE_CACHE_SIZE:=64m} -XX:MaxMetaspaceSize=${JAVA_MAX_META_SPACE_SIZE:=256m} -Xms${JAVA_XMS:=256m} -Xmx${JAVA_XMX:=450m} -Djava.security.egd=file:/dev/./urandom -jar /app.jar"