variable "name" {
  description = "The name of this ECR"
}

variable "principals_full_access" {
  description = "Who can have full access to this ECR"
}