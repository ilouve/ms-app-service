package br.com.frwkapp.abstracts;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.frwkapp.model.domain.Integrante;
import br.com.frwkapp.model.domain.Tenant;
import br.com.frwkapp.model.domain.User;
import br.com.frwkapp.model.service.IntegranteService;
import br.com.frwkapp.model.service.UserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

public abstract class BaseRestController<E extends BaseEntity, D extends BaseDTO> {

	@Autowired
	private UserService userService;
	@Autowired
	private IntegranteService employeeService;

	protected abstract BaseService<E, D> getService();

	protected User me() {
		return userService.findByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
	}

	protected Tenant getTenant() {
		try {
			User user = me();
			return user.getTenant();
		} catch (Throwable t) {
		}
		return null;
	}
	
	protected Integrante getEmployee() {
		User user = me();
		return employeeService.findOneByUser(user);
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Find all")
	@RequestMapping(method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    public List<D> findAll(@RequestParam(value = "search", required = false) String search) {
		List<E> result = getService().findAll(getTenant(), search);
		return getService().parseToDTO(result);
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Find page")
	@RequestMapping(value = "/page", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "page", value = "Current Page", dataType = "int", paramType = "query"),
		@ApiImplicitParam(name = "size", value = "Page Size", dataType = "int", paramType = "query"),
		@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	})
    public Page<D> findAll(@RequestParam(value = "search", required = false) String search,
    		@RequestParam(value = "page", required = false) Integer page,
    		@RequestParam(value = "size", required = false) Integer size) {
		Page<E> result = getService().findAll(getTenant(), search, PageRequest.of(page, size));
		return getService().parseToDTO(result);
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Find one by id")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "id of your entity", dataType = "long", paramType = "path") })
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    public D findOne(@PathVariable Long id) {
		E entity = getService().findOne(getTenant(), id);
		return getService().parseToDTO(entity);
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Create new")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    public D createNew(@RequestBody @Valid D dto) {

		E entity = getService().parseDtoToEntity(dto);
	    entity.setUserUpdate(me().getName());
		getService().insert(entity);

		return getService().parseToDTO(entity);
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Update an existing")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json;charset=UTF-8")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    public D update(@RequestBody @Valid D dto) {
		
		E entity = getService().parseDtoToEntity(dto);
		entity.setUserUpdate(me().getName());
		getService().update(entity);
		
		return getService().parseToDTO(entity);
	}

	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Delete one")
	@ApiImplicitParams({@ApiImplicitParam(name = "id", value = "id of your entity", dataType = "long", paramType = "path")})
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "application/json;charset=UTF-8")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    public void deleteOne(@PathVariable Long id) {
		getService().delete(getTenant(), id);
	}

	protected BaseResponseDTO buildResponse(Object object) {
		BaseResponseDTO response = new BaseResponseDTO();
		response.setData(object);
		return response;
	}

	protected BaseResponseDTO buildResponseMessage(String message) {
		BaseResponseDTO response = new BaseResponseDTO();
		response.setMessage(message);
		return response;
	}
}
