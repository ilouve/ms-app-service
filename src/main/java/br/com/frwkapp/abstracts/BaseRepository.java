package br.com.frwkapp.abstracts;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import br.com.frwkapp.model.domain.Tenant;

@NoRepositoryBean
public interface BaseRepository<E extends BaseEntity> extends JpaRepository<E, Long> {

	E findFirstByTenantAndId(Tenant tenant, Long id);
	List<E> findAllByTenantOrderByIdDesc(Tenant tenant);
	Page<E> findAllByTenantOrderByIdDesc(Tenant tenant, Pageable pageable);
    List<E> findAllByTenantAndSearchContainingIgnoreCaseOrderByIdDesc(Tenant tenant, String indice);
    Page<E> findAllByTenantAndSearchContainingIgnoreCaseOrderByIdDesc(Tenant tenant, String indice, Pageable pageable);
    void deleteByTenantAndId(Tenant tenant, Long id);
}
