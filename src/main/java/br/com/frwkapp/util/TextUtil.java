package br.com.frwkapp.util;

import java.text.ParseException;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.swing.text.MaskFormatter;

import org.apache.commons.lang.StringUtils;

import com.google.common.base.Optional;

import lombok.extern.java.Log;

@Log
public class TextUtil {

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);


    public static String rightPadding(String value, int size) {
        return StringUtils.rightPad(value, size, " ");
    }

    public static String leftPadding(String value, int size) {
        return StringUtils.leftPad(value, size, " ");
    }

    public static String rightPadding(String value, int size, String replace) {
        return StringUtils.rightPad(value, size, replace);
    }

    public static String leftPadding(String value, int size, String replace) {
        return StringUtils.leftPad(value, size, replace);
    }

    public static String firstName(String name) {
        if (StringUtils.isBlank(name)) {
            return "";
        }
        return StringUtils.capitalize(name.split(" ")[0].toLowerCase());
    }

    public static boolean validEmail(String email) {
        return VALID_EMAIL_ADDRESS_REGEX.matcher(email).find();
    }

    /**
     * Mask Examples
     * <p>
     * "#####-###" => "81580200" = "81580-200"
     * "###.###.###-##" => "01234569905" = "012.345.699-01"
     * "##.###.###/####-##" => "01234569905234" = 01.234.569/9052-34
     *
     * @param pattern
     * @param value
     * @return
     */
    public static String applyMask(String pattern, String value) {
        if (StringUtils.isBlank(pattern) || StringUtils.isBlank(value)) {
            log.severe("Invalid Params - pattern: " + pattern + " value:" + value);
            return "";
        }

        try {

            MaskFormatter mask = new MaskFormatter(pattern);
            mask.setValueContainsLiteralCharacters(false);
            return mask.valueToString(value.replaceAll("[^0-9]", ""));

        } catch (ParseException e) {
            log.severe("Parse Error - pattern: " + pattern + " value:" + value);
        }
        return "";
    }

    public static String coalesce(String value, String or) {
        if (StringUtils.isBlank(value) && StringUtils.isBlank(or)) {
            return "";
        }
        return Optional.fromNullable(value).or(or);
    }

    public static String generateRandomCode() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    public static boolean validCard(final String cardNumber) {
        if (StringUtils.isBlank(cardNumber) || cardNumber.length() < "4444333322221111".length()) {
            return false;
        }

        int sum = 0;
        boolean alternate = false;
        final String value = cardNumber.replaceAll("[^0-9]", "");
        for (int index = value.length() - 1; index >= 0; index--) {
            int digit = Integer.parseInt(value.substring(index, index + 1));
            if (alternate) {
                digit *= 2;
                if (digit > 9) {
                    digit = (digit % 10) + 1;
                }
            }
            sum += digit;
            alternate = !alternate;
        }

        return (sum % 10 == 0);
    }

    public static boolean containsLetter(String value) {
        for (int i = 0; i != value.length(); ++i) {
            if (Character.isLetter(value.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    public static String keepOnlyNumbers(String value) {
        if (StringUtils.isBlank(value)) return "";

        return value.replaceAll("[^0-9]", "");
    }

}
