package br.com.frwkapp.util;

import java.util.Date;
import java.util.UUID;

import org.apache.tomcat.util.codec.binary.Base64;
import org.apache.tomcat.util.codec.binary.StringUtils;

import com.google.common.base.Optional;

public class ApplicationUtil {

    public static String decode(String encode) {
        return StringUtils.newStringUtf8(Base64.decodeBase64(encode));
    }

    public static String coalesce(String nullable, String or) {
        return Optional.fromNullable(nullable).or(or);
    }

    public static Boolean coalesce(Boolean nullable, Boolean or) {
        return Optional.fromNullable(nullable).or(or);
    }

    public static String generateOSCode() {
        return UUID.randomUUID().toString().substring(27, 36).toUpperCase();
    }

    public static Date millisToDate(Long millis) {
        if (millis == null) {
            millis = 0L;
        }
        if (millis < 1000000000000L) {
            millis = millis * 1000;
        }
        return new Date(millis);
    }
}