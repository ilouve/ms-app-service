package br.com.frwkapp.model.repository;

import org.springframework.stereotype.Repository;

import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.model.domain.Integrante;
import br.com.frwkapp.model.domain.User;

@Repository
public interface IntegranteRepository extends BaseRepository<Integrante> {

	Integrante findOneByUser(User user);
}