package br.com.frwkapp.model.repository;

import org.springframework.stereotype.Repository;

import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.model.domain.Posicao;

@Repository
public interface PosicaoRepository extends BaseRepository<Posicao> {

}