package br.com.frwkapp.model.repository;

import org.springframework.stereotype.Repository;

import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.model.domain.User;

@Repository
public interface UserRepository extends BaseRepository<User> {

	public User findByLoginAndPassword(String login, String password);
	public User findByLogin(String login);
}