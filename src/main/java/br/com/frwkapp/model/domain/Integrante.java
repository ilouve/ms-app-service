package br.com.frwkapp.model.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.frwkapp.abstracts.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Entity
@Table(name = "t_integrante")
public class Integrante extends BaseEntity {

	@Id
	@GeneratedValue(generator = "sq_integrante")
	@SequenceGenerator(name = "sq_integrante", sequenceName = "sq_integrante", allocationSize = 1)
	@Column(name = "id")
	private Long id;

	@Column(name = "nome")
	private String nome;
	
	@Column(name = "email")
	private String email;

	@ManyToOne
	@JoinColumn(name = "fk_user")
	private User user;

	@ManyToMany
	@JoinTable(name = "t_integrante_posicao", joinColumns = @JoinColumn(name = "fk_integrante"), inverseJoinColumns = @JoinColumn(name = "fk_posicao"))
	private List<Posicao> posicoes = new ArrayList<>();

	public Integrante(Long id) {
		this.id = id;
	}

}