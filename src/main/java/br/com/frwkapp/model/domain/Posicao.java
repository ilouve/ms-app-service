package br.com.frwkapp.model.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.frwkapp.abstracts.BaseEntity;
import br.com.frwkapp.abstracts.SearchField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Entity
@Table(name = "t_posicao")
public class Posicao extends BaseEntity {

    @Id
    @GeneratedValue(generator = "sq_posicao")
    @SequenceGenerator(name = "sq_posicao", sequenceName = "sq_posicao", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @SearchField
    @Column(name = "name")
    private String nome;
    
    public Posicao(Long id) {
    	this.id = id;
    }
}