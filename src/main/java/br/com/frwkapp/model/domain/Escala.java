package br.com.frwkapp.model.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.frwkapp.abstracts.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "t_escala")
public class Escala extends BaseEntity {


    @Id
    @GeneratedValue(generator = "sq_escala")
    @SequenceGenerator(name = "sq_escala", sequenceName = "sq_escala", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @Column(name = "nome")
    private String nome;
    
    @ManyToOne
    @JoinColumn(name = "fk_integrante")
    private Integrante integrante;
    
    @ManyToOne
    @JoinColumn(name = "fk_posicao")
    private Posicao posicao;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data")
    private Date data;
    
}