package br.com.frwkapp.model.exception;

public class UserDisabledException extends Exception {
	public UserDisabledException() {
		super("Usuário Inativo.");
	}

}
