package br.com.frwkapp.model.dto;

import br.com.frwkapp.abstracts.BaseDTO;
import br.com.frwkapp.model.domain.Posicao;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class PosicaoDTO extends BaseDTO {

	private String nome;

    public PosicaoDTO(Posicao entity) {
        super(entity);
        this.nome = entity.getNome();
    }
 
    public PosicaoDTO() {}
}