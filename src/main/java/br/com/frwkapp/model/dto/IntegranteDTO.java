package br.com.frwkapp.model.dto;

import java.util.List;
import java.util.stream.Collectors;

import br.com.frwkapp.abstracts.BaseDTO;
import br.com.frwkapp.model.domain.Integrante;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class IntegranteDTO extends BaseDTO {

	private String nome;
	private String email;
	private List<PosicaoDTO> posicoes;

    public IntegranteDTO(Integrante entity) {
        super(entity);
        this.nome = entity.getNome();
        this.email = entity.getEmail();
        if (entity.getPosicoes() != null) {
        	posicoes = entity.getPosicoes().stream().map(PosicaoDTO::new).collect(Collectors.toList());
        }
    }
 
    public IntegranteDTO() {}
}