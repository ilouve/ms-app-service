package br.com.frwkapp.model.dto;

import java.util.List;

import br.com.frwkapp.abstracts.BaseDTO;
import br.com.frwkapp.model.domain.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class UserDTO extends BaseDTO {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String login;
    private String password;
    private String name;
    private List<String> roles;

    public UserDTO(User entity) {
        super(entity);
        this.login = entity.getLogin();
        this.password = entity.getPassword();
        this.name = entity.getName();
        this.roles = entity.getRoles();
    }
 
    public UserDTO() {}
}