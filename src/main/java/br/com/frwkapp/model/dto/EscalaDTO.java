package br.com.frwkapp.model.dto;

import java.util.Date;

import br.com.frwkapp.abstracts.BaseDTO;
import br.com.frwkapp.model.domain.Escala;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class EscalaDTO extends BaseDTO {

	private String nome;
	private IntegranteDTO integrante;
	private PosicaoDTO posicao;
	private Date data;

    public EscalaDTO(Escala entity) {
        super(entity);
        this.nome = entity.getNome();
        if (entity.getIntegrante() != null) {
        	this.integrante = new IntegranteDTO(entity.getIntegrante());
        }
        if (entity.getPosicao() != null) {
        	this.posicao = new PosicaoDTO(entity.getPosicao());
        }
        this.data = entity.getData();
    }
 
    public EscalaDTO() {}
}