package br.com.frwkapp.model.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.abstracts.BaseService;
import br.com.frwkapp.model.domain.Escala;
import br.com.frwkapp.model.domain.Integrante;
import br.com.frwkapp.model.domain.Posicao;
import br.com.frwkapp.model.domain.User;
import br.com.frwkapp.model.dto.EscalaDTO;
import br.com.frwkapp.model.repository.EscalaRepository;

@Service
public class EscalaService extends BaseService<Escala, EscalaDTO> {

	@Autowired
	private EscalaRepository repository;
	@Autowired
	private UserService userService;
	@Autowired
	private IntegranteService IntegranteService;

	@Override
	public BaseRepository<Escala> getRepository() {
		return repository;
	}

	@Override
	public Escala parseDtoToEntity(EscalaDTO dto) {
		Escala entity = new Escala();
		entity.setId(dto.getId());
		entity.setNome(dto.getNome());
		if (dto.getIntegrante() != null) {
			entity.setIntegrante(new Integrante(dto.getIntegrante().getId()));
		} else {
			User user = userService.findByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
			Integrante Integrante = IntegranteService.findOneByUser(user);
			entity.setIntegrante(Integrante);
		}
		if (dto.getPosicao() != null) {
			entity.setPosicao(new Posicao(dto.getPosicao().getId()));
		}
		entity.setData(dto.getData());
		return entity;
	}
	
	@Override
    public List<EscalaDTO> parseToDTO(List<Escala> list) {
        return list.stream().map(EscalaDTO::new).collect(Collectors.toList());
    }

    @Override
    public Page<EscalaDTO> parseToDTO(Page<Escala> page) {
        return page.map(EscalaDTO::new);
    }

    @Override
    public EscalaDTO parseToDTO(Escala entity) {
    	if (entity == null) {
    		return null;
    	}
        return new EscalaDTO(entity);
    }
    
}
