package br.com.frwkapp.model.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.abstracts.BaseService;
import br.com.frwkapp.model.domain.Integrante;
import br.com.frwkapp.model.domain.Posicao;
import br.com.frwkapp.model.domain.User;
import br.com.frwkapp.model.dto.IntegranteDTO;
import br.com.frwkapp.model.repository.IntegranteRepository;

@Service
public class IntegranteService extends BaseService<Integrante, IntegranteDTO> {

	@Autowired
	private IntegranteRepository repository;

	@Override
	public BaseRepository<Integrante> getRepository() {
		return repository;
	}

	@Override
	public Integrante parseDtoToEntity(IntegranteDTO dto) {
		Integrante entity = new Integrante();
		entity.setId(dto.getId());
		entity.setNome(dto.getNome());
		entity.setEmail(dto.getEmail());
		if (dto.getPosicoes() != null) {
			dto.getPosicoes().forEach(p -> {
				entity.getPosicoes().add(new Posicao(p.getId()));
			});
		}
		return entity;
	}
	
	@Override
    public List<IntegranteDTO> parseToDTO(List<Integrante> list) {
        return list.stream().map(IntegranteDTO::new).collect(Collectors.toList());
    }

    @Override
    public Page<IntegranteDTO> parseToDTO(Page<Integrante> page) {
        return page.map(IntegranteDTO::new);
    }

    @Override
    public IntegranteDTO parseToDTO(Integrante entity) {
        return new IntegranteDTO(entity);
    }
    
    public Integrante findOneByUser(User user) {
    	return repository.findOneByUser(user);
    }
}
