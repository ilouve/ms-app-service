package br.com.frwkapp.model.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.abstracts.BaseService;
import br.com.frwkapp.model.domain.User;
import br.com.frwkapp.model.dto.UserDTO;
import br.com.frwkapp.model.exception.UserNotFoundException;
import br.com.frwkapp.model.exception.UserDisabledException;
import br.com.frwkapp.model.repository.UserRepository;
import br.com.frwkapp.security.LoginResultDTO;
import br.com.frwkapp.security.TokenService;

@Service
public class UserService extends BaseService<User, UserDTO> {

	@Autowired
	private UserRepository repository;

	@Autowired
	private TokenService tokenService;

	@Override
	public BaseRepository<User> getRepository() {
		return repository;
	}

	@Override
	public User parseDtoToEntity(UserDTO dto) {
		User entity = new User();
		entity.setId(dto.getId());
		entity.setLogin(dto.getLogin());
		entity.setPassword(dto.getPassword());
		entity.setName(dto.getName());
		return entity;
	}
	
	@Override
    public List<UserDTO> parseToDTO(List<User> list) {
        return list.stream().map(UserDTO::new).collect(Collectors.toList());
    }

    @Override
    public Page<UserDTO> parseToDTO(Page<User> page) {
        return page.map(UserDTO::new);
    }

    @Override
    public UserDTO parseToDTO(User entity) {
        return new UserDTO(entity);
    }

	public LoginResultDTO login(String login, String senha, String tokenFirebase) throws Exception {
		User user = repository.findByLoginAndPassword(login, senha);
		
		if (user == null) {
			throw new UserNotFoundException();
		}
		if (user.isDeleted()) {
			throw new UserDisabledException();
		}

		LoginResultDTO dto = tokenService.createTokenForUser(user);
		dto.setUserId(user.getId());
		dto.setUserName(user.getName());
		if (tokenFirebase != null && !user.getDevices().contains(tokenFirebase)) {
			user.getDevices().add(tokenFirebase);
		}
		repository.save(user);
		return dto;
	}

	public LoginResultDTO refreshLogin(String refreshToken) {
		return tokenService.refreshToken(refreshToken);
	}

	public User findByLogin(String login) {
		return repository.findByLogin(login);
	}
}
