package br.com.frwkapp.model.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.abstracts.BaseService;
import br.com.frwkapp.model.domain.Posicao;
import br.com.frwkapp.model.dto.PosicaoDTO;
import br.com.frwkapp.model.repository.PosicaoRepository;

@Service
public class PosicaoService extends BaseService<Posicao, PosicaoDTO> {

	@Autowired
	private PosicaoRepository repository;

	@Override
	public BaseRepository<Posicao> getRepository() {
		return repository;
	}

	@Override
	public Posicao parseDtoToEntity(PosicaoDTO dto) {
		Posicao entity = new Posicao();
		entity.setId(dto.getId());
		entity.setNome(dto.getNome());
		return entity;
	}
	
	@Override
    public List<PosicaoDTO> parseToDTO(List<Posicao> list) {
        return list.stream().map(PosicaoDTO::new).collect(Collectors.toList());
    }

    @Override
    public Page<PosicaoDTO> parseToDTO(Page<Posicao> page) {
        return page.map(PosicaoDTO::new);
    }

    @Override
    public PosicaoDTO parseToDTO(Posicao entity) {
        return new PosicaoDTO(entity);
    }
    
}
