package br.com.frwkapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.frwkapp.abstracts.BaseRestController;
import br.com.frwkapp.abstracts.BaseService;
import br.com.frwkapp.model.domain.Integrante;
import br.com.frwkapp.model.dto.IntegranteDTO;
import br.com.frwkapp.model.service.IntegranteService;

@RestController
@RequestMapping("integrante")
public class IntegranteController extends BaseRestController<Integrante, IntegranteDTO> {

	@Autowired
    private IntegranteService service;
    
	@Override
    protected BaseService<Integrante, IntegranteDTO> getService() {
        return service;
    }
	
}