package br.com.frwkapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.frwkapp.abstracts.BaseRestController;
import br.com.frwkapp.abstracts.BaseService;
import br.com.frwkapp.model.domain.Escala;
import br.com.frwkapp.model.dto.EscalaDTO;
import br.com.frwkapp.model.service.EscalaService;

@RestController
@RequestMapping("escala")
public class EscalaController extends BaseRestController<Escala, EscalaDTO> {

	@Autowired
    private EscalaService service;
    
	@Override
    protected BaseService<Escala, EscalaDTO> getService() {
        return service;
    }
}