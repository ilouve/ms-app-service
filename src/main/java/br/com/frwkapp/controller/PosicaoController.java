package br.com.frwkapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.frwkapp.abstracts.BaseRestController;
import br.com.frwkapp.abstracts.BaseService;
import br.com.frwkapp.model.domain.Posicao;
import br.com.frwkapp.model.dto.PosicaoDTO;
import br.com.frwkapp.model.service.PosicaoService;

@RestController
@RequestMapping("posicao")
public class PosicaoController extends BaseRestController<Posicao, PosicaoDTO> {

	@Autowired
    private PosicaoService service;
    
	@Override
    protected BaseService<Posicao, PosicaoDTO> getService() {
        return service;
    }
	
}