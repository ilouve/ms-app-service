var BASE_AUTH_URL = "http://localhost:12000/login";
var BASE_URL = "http://localhost:12000/app";
var LIMIT = 10;
var COUNT_REQUEST = 0;
function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}
function isNull(param) {
	if (param == undefined || param == null || param == "") {
		return true
	} else {
		return false;
	}
}
(function () { "use strict";

    angular
    .module('appILouve', [ 'ui.router','ui.materialize', 'ngCookies', 'ngResource', 'ngSanitize', 'ngTagsInput'])
    .factory('BearerAuthInterceptor', function ($window, $q) {
        return {
            request: function(config) {
            	COUNT_REQUEST++;
            	if ($('#body').hasClass('loaded')) {
            		$('.principal').show();
            	}
                config.headers = config.headers || {};
                if ($window.localStorage.getItem('token_ilouve')) {
                    config.headers.Authorization = 'Bearer ' + $window.localStorage.getItem('token_ilouve');
                }
                return config || $q.when(config);
            },
            response: function(response) {
            	COUNT_REQUEST--;
            	if (COUNT_REQUEST <= 0) {
            		$('.principal').fadeOut();
            	}
            	return response;
            },
            responseError: function(rejection) {
            	COUNT_REQUEST--;
            	if (COUNT_REQUEST <= 0) {
            		$('.principal').fadeOut();
            	}
            	if (rejection.status == 401 || rejection.status == 403) {
            		$window.location.href = 'user-login.html';
            	}
            	return rejection;
            }
        }
    })
    .config(["$httpProvider", "$stateProvider", "$urlRouterProvider", function($httpProvider, $stateProvider, $urlRouterProvider) {
    	$urlRouterProvider.otherwise('');
        $httpProvider.interceptors.push('BearerAuthInterceptor');
    }]);
    
    angular.module('appILouve').controller('leftMenuController', leftMenuController);
    leftMenuController.$inject = ["$rootScope", "$scope", "$state", "$http", "$timeout", '$window'];
    function leftMenuController($rootScope, $scope, $state, $http, $timeout, $window) {
    	$rootScope.LIMIT = LIMIT;
        $http.get(BASE_AUTH_URL + "/auth/me").then(
            function (response) {
            	$rootScope.usuarioLogado = response.data.data;
            	$scope.usuarioLogado = response.data.data;
            }
        );
        
        $scope.logout = function() {
        	$window.localStorage.removeItem('token_ilouve');
            $window.localStorage.removeItem('usuario_ilouve');
            $window.location.href = 'user-login.html';
        }
        $rootScope.previousState;
        $rootScope.currentState;
        $rootScope.$on('$stateChangeSuccess', function(ev, to, toParams, from, fromParams) {
            $rootScope.previousState = from.name;
            $rootScope.currentState = to.name;
        });
    }
})()
