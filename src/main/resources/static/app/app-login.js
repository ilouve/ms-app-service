var BASE_URL = "http://localhost:12000/login";
//var BASE_URL = "http://thegameapp.net/rest";
var USUARIO_LOGADO;
var LIMIT = 10;
var COUNT_REQUEST = 0;
function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}
function isNull(param) {
	if (param == undefined || param == null || param == "") {
		return true
	} else {
		return false;
	}
}
(function () { "use strict";

    angular
    .module('appILouveLogin', [ 'ui.router','ui.materialize', 'ngCookies', 'ngResource'])
    .factory('BearerAuthInterceptor', function ($window, $q) {
        return {
            request: function(config) {
            	COUNT_REQUEST++;
            	if ($('#body').hasClass('loaded')) {
            		$('.principal').show();
            	}
                config.headers = config.headers || {};
                if ($window.localStorage.getItem('token_ilouve')) {
                    // may also use sessionStorage
                    config.headers.Authorization = 'Bearer ' + $window.localStorage.getItem('token_ilouve');
                    config.headers.USUARIO_LOGADO = $window.localStorage.getItem('usuario_ilouve');
                }
                return config || $q.when(config);
            },
            response: function(response) {
            	COUNT_REQUEST--;
            	if (COUNT_REQUEST <= 0) {
            		$('.principal').fadeOut();
            	}
            	return response;
            },
            responseError: function(rejection) {
            	COUNT_REQUEST--;
            	if (COUNT_REQUEST <= 0) {
            		$('.principal').fadeOut();
            	}
            	if (rejection.status == 401 || rejection.status == 403) {
            		$window.location.href = '/user-login.html';
            	}
            	return rejection;
            }
        }
    })
    .config(["$httpProvider", "$stateProvider", "$urlRouterProvider", "$transitionsProvider", function($httpProvider, $stateProvider, $urlRouterProvider, $transitionsProvider) {
    	$urlRouterProvider.otherwise('');
        $httpProvider.interceptors.push('BearerAuthInterceptor');
    }]);
    
    angular.module('appILouveLogin').directive('ngEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if(event.which === 13) {
                    scope.$apply(function() {
                        scope.$eval(attrs.ngEnter);
                    });
                    
                    event.preventDefault();
                }
            });
        };
    });
    
    angular
    .module('appILouveLogin')
    .controller('loginController', loginController);
    loginController.$inject = [ "$scope", "$http", "$state", "$stateParams", "$cookies", "$window"];

    function loginController($scope, $http, $state, $stateParams, $cookies, $window) {
    	$window.localStorage.removeItem('token_ilouve');
        $window.localStorage.removeItem('usuario_ilouve');
        $scope.login = function(user) {
            if (!user) {
                swal("It's Bad!", "Informe o usuário e senha!", "error")
                return;
            }
            var req = {
                url    : BASE_URL+'/auth',
                data   : user,
                method : 'POST'
            };
            $http(req)
            .then(function (response) {
            		if (response.status == 200) {
            			$window.localStorage.setItem('token_ilouve', response.data.data.token);
            			$window.localStorage.setItem('usuario_ilouve', JSON.stringify(response.data.data));
            			$window.location.href = '/index.html';
            		} else {
            			swal("Ops!", response.data.message, "warning")
            		}
            },
            function (error) {
                swal("It's Bad!", "Usuário não logado!", "error")
            });
        }
    };
})()