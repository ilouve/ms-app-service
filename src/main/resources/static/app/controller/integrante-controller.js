(function () {
    "use strict";
    /* ############################################### LIST CONTROLLER #############################################*/
    angular.module('appILouve').controller('listIntegranteController', listIntegranteController);
    listIntegranteController.$inject = [ "$scope", "$http", "$state"];
    function listIntegranteController($scope, $http, $state) {
        $scope.list = function (page) {
            $http.get(BASE_URL + "/integrante/page",
                {params : {page: page, size: LIMIT, search: $scope.search}}).then(
                function (response) {
                    $scope.listIntegrante = response.data.content;
                    $scope.total = response.data.totalElements;
                    $(".card-title").click();
                }
            );
        }
        $scope.list(0);

        $scope.novo = function () {
            $state.go('edit-integrante', {id: null});
        };
        $scope.edit = function (id) {
            $state.go('edit-integrante', {id: id});
        };
        $scope.delete = function (id) {
            swal({
            	title: "Tem certeza?",   
                text: "Você não poderá recuperar este registro se for apagado!",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Sim, apagar!",   
                cancelButtonText: "Não, cancelar!",   
                closeOnConfirm: false,   
                closeOnCancel: false}, 
                function(isConfirm) {
                    if (isConfirm) {
                        $http.delete(BASE_URL + "/integrante/"+id).then(
                            function (response) {
                                for (var i =0; i < $scope.listIntegrante.length; i++)
                                    if ($scope.listIntegrante[i].id == id) {
                                        $scope.listIntegrante.splice(i,1);
                                        break;
                                    }
                                swal("Apagado", "Seu registro foi apagado!.", "success");  
                            }
                        );
                    }
               });
        };
        
    }

    /* ############################################### EDIT CONTROLLER #############################################*/
    angular.module('appILouve').controller('editIntegranteController', editIntegranteController);
    editIntegranteController.$inject = [ "$scope", "$http", "$state", "$stateParams"];
    function editIntegranteController($scope, $http, $state, $stateParams) {
    	$('#posicao').select2();
        if (!isNull($stateParams.id)) {
            $http.get(BASE_URL + "/integrante/"+$stateParams.id).then(
                function (response) {
                    $scope.integrante = response.data;
                    buscaPosicoes();
                }
            );
        } else {
        	buscaPosicoes();
        }

        $scope.voltar = function () {
            $state.go('integrante');
        };
        $scope.salvar = function (integrante) {
            if (!isNull(integrante.id)) {
                var req = {
                    method: 'PUT',
                    url: BASE_URL+'/integrante',
                    data: integrante
                };
                $http(req).then(
                    function (integrante) {
                        swal("Good job!", "Registro atualizado com sucesso!", "success")
                    }
                );
            } else {
                var req = {
                    method: 'POST',
                    url: BASE_URL+'/integrante',
                    data: integrante
                };
                $http(req).then(
                    function (response) {
                        swal("Good job!", "Registro salvo com sucesso!", "success")
                    }
                );
            }
        };
        
        function buscaPosicoes() {
        	$http.get(BASE_URL + "/posicao").then(
                function (response) {
                    $scope.posicoes = [];
        			if($scope.integrante != null && $scope.integrante.posicoes != null && $scope.integrante.posicoes != undefined && $scope.integrante.posicoes.length > 0){
                    	angular.forEach(response.data, function(value, key) {
                    		if ($scope.integrante.posicoes.filter(e => e.id === value.id).length == 0) {
                    			$scope.posicoes.push(value);
                			}
                    	});
        			} else {
                    	$scope.posicoes = response.data;
        			}
                }
            );
        }
        
        
        $scope.removerPosicoes = function() {
		    angular.forEach($scope.integrante.posicoes, function(value, key) {
		    	$scope.posicoes.push(value)
		    });
		    $scope.posicoes.sort(function(a,b) {
		        return a.nome < b.nome ? -1 : a.nome > b.nome ? 1 : 0;
		    });
      	   $scope.integrante.posicoes = [];
        }

        $scope.removerPosicao = function(id) {
		    var canAdd = true;
		    angular.forEach($scope.integrante.posicoes, function(value, key) {
		    	if(value.id === id) {
		    		$scope.integrante.posicoes.splice(key, 1)
		    		$scope.posicoes.push(value);
		    		return;
		    	}
	    	});
		    $scope.posicoes.sort(function(a,b) {
		        return a.nome < b.nome ? -1 : a.nome > b.nome ? 1 : 0;
		    });
		    return canAdd;
	    }

        $scope.adicionarPosicao = function (posicao) {
        	if ($scope.integrante.posicoes == undefined) {
        		$scope.integrante.posicoes = [];
        	}
        	var index = 0;
        	var achou = false;
        	angular.forEach($scope.posicoes, function(value, key) {
  			   if (value.id === posicao && _validateAddPosicao(value.id)) {
  				   $scope.integrante.posicoes.push(value);
  				   achou = true;
  			   }
  			   if (!achou) {
  				   index++;
  			   }
  		    });
        	$scope.posicoes.splice(index, 1);
        	
        };

        var _validateAddPosicao = function(id) {
		    var canAdd = true;
		    angular.forEach($scope.integrante.posicoes, function(value, key) {
		    	if(value.id === id) {
		    		canAdd = false;
		    		return;
		    	}
	    	});
		    return canAdd;
	    }
    }
})()
