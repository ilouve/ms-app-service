(function () {
    "use strict";
    /* ############################################### LIST CONTROLLER #############################################*/
    angular.module('appILouve').controller('listEscalaController', listEscalaController);
    listEscalaController.$inject = [ "$scope", "$http", "$state"];
    function listEscalaController($scope, $http, $state) {
        $scope.list = function (page) {
            $http.get(BASE_URL + "/escala/page",
                {params : {page: page, size: LIMIT, search: $scope.search}}).then(
                function (response) {
                    $scope.listEscala = response.data.content;
                    $scope.total = response.data.totalElements;
                    $(".card-title").click();
                }
            );
        }
        $scope.list(0);

        $scope.novo = function () {
            $state.go('edit-escala', {id: null});
        };
        $scope.edit = function (id) {
            $state.go('edit-escala', {id: id});
        };
        $scope.delete = function (id) {
            swal({
            	title: "Tem certeza?",   
                text: "Você não poderá recuperar este registro se for apagado!",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Sim, apagar!",   
                cancelButtonText: "Não, cancelar!",   
                closeOnConfirm: false,   
                closeOnCancel: false}, 
                function(isConfirm) {
                    if (isConfirm) {
                        $http.delete(BASE_URL + "/escala/"+id).then(
                            function (response) {
                                for (var i =0; i < $scope.listEscala.length; i++)
                                    if ($scope.listEscala[i].id == id) {
                                        $scope.listEscala.splice(i,1);
                                        break;
                                    }
                                swal("Apagado", "Seu registro foi apagado!.", "success");  
                            }
                        );
                    }
               });
        };
        
    }

    /* ############################################### EDIT CONTROLLER #############################################*/
    angular.module('appILouve').controller('editEscalaController', editEscalaController);
    editEscalaController.$inject = [ "$scope", "$http", "$state", "$stateParams"];
    function editEscalaController($scope, $http, $state, $stateParams) {
        $scope.title = "Escala";
        if (!isNull($stateParams.id)) {
            $http.get(BASE_URL + "/escala/"+$stateParams.id).then(
                function (response) {
                    $scope.escala = response.data;
                }
            );
        }

        $scope.voltar = function () {
            $state.go('escala');
        };
        $scope.salvar = function (escala) {
            if (!isNull(escala.id)) {
                var req = {
                    method: 'PUT',
                    url: BASE_URL+'/escala',
                    data: escala
                };
                $http(req).then(
                    function (escala) {
                        swal("Good job!", "Registro atualizado com sucesso!", "success")
                    }
                );
            } else {
                var req = {
                    method: 'POST',
                    url: BASE_URL+'/escala',
                    data: escala
                };
                $http(req).then(
                    function (response) {
                        swal("Good job!", "Registro salvo com sucesso!", "success")
                    }
                );
            }
        };
    }
})()
