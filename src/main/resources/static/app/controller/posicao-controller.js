(function () {
    "use strict";
    /* ############################################### LIST CONTROLLER #############################################*/
    angular.module('appILouve').controller('listPosicaoController', listPosicaoController);
    listPosicaoController.$inject = [ "$scope", "$http", "$state"];
    function listPosicaoController($scope, $http, $state) {
        $scope.list = function (page) {
            $http.get(BASE_URL + "/posicao/page",
                {params : {page: page, size: LIMIT, search: $scope.search}}).then(
                function (response) {
                    $scope.listPosicao = response.data.content;
                    $scope.total = response.data.totalElements;
                    $(".card-title").click();
                }
            );
        }
        $scope.list(0);

        $scope.novo = function () {
            $state.go('edit-posicao', {id: null});
        };
        $scope.edit = function (id) {
            $state.go('edit-posicao', {id: id});
        };
        $scope.delete = function (id) {
            swal({
            	title: "Tem certeza?",   
                text: "Você não poderá recuperar este registro se for apagado!",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Sim, apagar!",   
                cancelButtonText: "Não, cancelar!",   
                closeOnConfirm: false,   
                closeOnCancel: false}, 
                function(isConfirm) {
                    if (isConfirm) {
                        $http.delete(BASE_URL + "/posicao/"+id).then(
                            function (response) {
                                for (var i =0; i < $scope.listPosicao.length; i++)
                                    if ($scope.listPosicao[i].id == id) {
                                        $scope.listPosicao.splice(i,1);
                                        break;
                                    }
                                swal("Apagado", "Seu registro foi apagado!.", "success");  
                            }
                        );
                    }
               });
        };
        
    }

    /* ############################################### EDIT CONTROLLER #############################################*/
    angular.module('appILouve').controller('editPosicaoController', editPosicaoController);
    editPosicaoController.$inject = [ "$scope", "$http", "$state", "$stateParams"];
    function editPosicaoController($scope, $http, $state, $stateParams) {
        $scope.title = "Posicao";
        if (!isNull($stateParams.id)) {
            $http.get(BASE_URL + "/posicao/"+$stateParams.id).then(
                function (response) {
                    $scope.posicao = response.data;
                }
            );
        }

        $scope.voltar = function () {
            $state.go('posicao');
        };
        $scope.salvar = function (posicao) {
            if (!isNull(posicao.id)) {
                var req = {
                    method: 'PUT',
                    url: BASE_URL+'/posicao',
                    data: posicao
                };
                $http(req).then(
                    function (posicao) {
                        swal("Good job!", "Registro atualizado com sucesso!", "success")
                    }
                );
            } else {
                var req = {
                    method: 'POST',
                    url: BASE_URL+'/posicao',
                    data: posicao
                };
                $http(req).then(
                    function (response) {
                        swal("Good job!", "Registro salvo com sucesso!", "success")
                    }
                );
            }
        };
    }
})()
