(function () { "use strict";
    angular
    .module('appILouve')
    .config(["$httpProvider", "$stateProvider", "$urlRouterProvider", function($httpProvider, $stateProvider, $urlRouterProvider) {
        /* #################################################################################################### */
        /* #####################################  ROTAS  ###################################################### */
        /* #################################################################################################### */
    	var posicaoState = {
            name : 'posicao',
            url : '/posicao',
            controller : 'listPosicaoController',
            templateUrl : 'app/view/list/list-posicao.html'
        }
        $stateProvider.state('posicao', posicaoState);
        
        var editPosicaoState = {
            name : 'edit-posicao',
            url : '/edit-posicao/:id',
            params : { id: undefined },
            controller : 'editPosicaoController',
            templateUrl : 'app/view/edit/edit-posicao.html'
        }
        $stateProvider.state('edit-posicao', editPosicaoState);
        
        var integranteState = {
            name : 'integrante',
            url : '/integrante',
            controller : 'listIntegranteController',
            templateUrl : 'app/view/list/list-integrante.html'
        }
        $stateProvider.state('integrante', integranteState);
        
        var editIntegranteState = {
            name : 'edit-integrante',
            url : '/edit-integrante/:id',
            params : { id: undefined },
            controller : 'editIntegranteController',
            templateUrl : 'app/view/edit/edit-integrante.html'
        }
        $stateProvider.state('edit-integrante', editIntegranteState);
        
        var escalaState = {
            name : 'escala',
            url : '/escala',
            controller : 'listEscalaController',
            templateUrl : 'app/view/list/list-escala.html'
        }
        $stateProvider.state('escala', escalaState);
        
        var editEscalaState = {
            name : 'edit-escala',
            url : '/edit-escala/:id',
            params : { id: undefined },
            controller : 'editEscalaController',
            templateUrl : 'app/view/edit/edit-escala.html'
        }
        $stateProvider.state('edit-escala', editEscalaState);
    	
	    /* #################################################################################################### */
	    /* #################################################################################################### */
    }]);
})()
